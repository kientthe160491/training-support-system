# S-Courses (Training Support Systems)
# About : 
   ### * Description : 
   S-Courses (Training Support System) is a software project built to help institutions, students, students and teachers manage their teaching and learning. For each different user object, there will be different functions and activities. All users have their accounts secured with User Authentication. A common feature for all users is to be able to login, register, view personal information, change password and retrieve password. S-Course will help users have a good experience, quick access, diverse features to help users monitor, manage and edit quickly. This system support for the below types of user
   - Guest: unregistered users
   - Trainee: register and/or be sent to participate in classes, courses, and learn lessons in the system
   - Trainer: manage students, plan class learning content, evaluate students learning process
   - Supporter: support teachers in implementing classes (class configuration, class schedule, class list), perform work with classes of subjects as assigned by manager (configured by class), support processing and replying to contacts from the web (Web contacts)
   - Manager: subject leader, in charge of the assigned subjects: subject configuration, class management
   - Admin: full authority, acts as the system administrator
   ### * Role/Main Tasks : Member/Backend( Must complete all requirements assigned (back-end and front-end of requirement )  )
   ### * Languages programing, technologies, libraries and frameworks : 
  - Java, JavaScript, Html, Css, Boostrap, MySQL
# My features are assigned: 
## 1. Admin
#### 1.1 Screen layout for User Details
Requirement: 
- This function allows admin to edit user information. 
![1.1 User Details](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image26.PNG)

## 2. Manager
#### 2.1 Screen layout for Subject List
Requirement: 
- Users can fill in the information in the input boxes to filter out the list at will
- Subject Code: is the subject code
- Subject Name: Name of the subject
- Subject Author: Is the name of the teacher who teaches or classifies the subject
- Status: Is the status of the subject
- Column detail: to navigate to the subject information edit page
- Pagination
- Filter by Manager, Expert, Status, Subject code or name
- Link to Add a new subject page
- Sort Ascending or Descending by column
![2.1 User Details](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image18.PNG)

#### 2.2 Screen layout for Subject Details
Requirement for Add new subject screen: 
- Manager can Create new Subject (Code, Name, Status, Manager, Expert) in Add subject screen
- When click the Update button, the web will notify the subject is updated with a link to get back to subject list screen
- Check empty subject code, subject name, status, ,manager, expert, description
- Click cancel button to go back to subject list screen
![2.2 Create Subject](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image16.PNG)

Requirement for Update subject screen: 
- Manager can update Subject Information (Code, Name, Status, Manager, Expert) in subject details
- When click the Update button, the web will notify the subject is updated with a link to get back to subject list screen
- Check empty subject code, subject name, status, ,manager, expert, description
- Click cancel button to go back to subject list screen

![2.2 Update Subject](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image27.PNG)

#### 2.3 Screen layout for Class List
Requirement: 
- Manager uses this function to manage classes infomation
- Column detail: to navigate to the class information edit page
- Pagination
- Filter by Term, Trainer, Status and Class code
- Sort Ascending or Descending by column

![2.3 Class List](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image9.PNG)

#### 2.4 Screen layout for Subject Setting List
Requirement: 
- Users can fill in the information in the input boxes to filter out the list
- Subject Code: Code of that subject setting
- Title: subject information: Complexity level, Quality Level
- Status: Is the status of the subject setting
- Column detail: to navigate to the subject setting information edit page
- Filter by Subject Code, Status, Title
- Pagination
- Sort Ascending or Descending by column
- Link to Add new Subject Setting page
- Link to Edit Subject Setting page

![2.4 Subject Setting](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image15.PNG)

#### 2.4 Screen layout for Subject Setting Details
Requirement for Add new Subject Setting: 
- Manager can create Subject Setting by using Add function
- Cancel to remove Create progress
- Click Create, the website will notify update successfully and click in the link to go back the Subject setting list
- Check valid input

![2.4 Subject Setting Details](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image38.PNG)

Requirement for Edit Subject Setting: 
- Manager can edit Subject Setting Details by using Update function
-Cancel to remove update progress
- Click Update, the website will notify update successfully and click in the link to go back the Subject setting list
- Check valid input

![2.4 Subject Setting Details](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image14.PNG)

## 3. Supporter
#### 3.1 Screen layout for Schedule List
Requirement: 
- View/Edit: link to the Schedule Details page to view/edit the relevant class schedule
- Take Attendance: link to Attendance Details page to add/edit the class attendance of the relevant slot - this link is available only durent the training day of that slot
- Global actions:
- View Attendance: link to Attendance Tracking page
- Add New: link to the Schedule Details page to add new class schedule
- The Trainer have similar permissions as the Supporter on this page

![3.1 Schedule List](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image6.PNG)

#### 3.3 Screen layout for Schedule Details
Requirement for Add new Schedule: 
- Supporter uses this function to create a new schedule 
- Cancel to remove Create progress
- Click Create, the website will notify update successfully and click in the link to go back the Schedule list
+ Check valid input

![3.3 Schedule Details](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image19.PNG)

Requirement for edit Schedule details: 
- Supporter uses this function to edit schedule information
- Edit Schedule information
- Cancel to remove Create progress
- Click Update, the website will notify update successfully and click in the link to go back the Schedule list
+ Check valid input

![3.3 Schedule Details](https://gitlab.com/kientthe160491/training-support-system/-/raw/main/image11.PNG)
